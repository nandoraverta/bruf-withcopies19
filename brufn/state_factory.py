from sortedcontainers import SortedList
from brufn.network import State

class StateFactory:

    def __init__(self, num_of_copies, sources):
        self._num_of_copies = num_of_copies
        self._states: SortedList[State] = None
        self._states_number: int = 0
        self._sources = sources

    def create_state(self, states, ts, allow_creation=True):
        state = State(states, ts, codified_param=self._num_of_copies)
        try:
            i = self._states.index(state)
            return self._states[i]
        except ValueError:
            if allow_creation and (state.ts > 0 or any(state.states[x] == self._num_of_copies for x in self._sources)):
                self._states.add(state)
                self._states_number += 1
                #print("Create State %d: %s"%(state.id, state))
                return state
            else:
                #print("StateFactory::create_state : state %s is not reachable."%(str(state)))
                return None

    def empty_state_list(self):
        del self._states
        self._states = SortedList()

    def set_state_list(self, list):
        del self._states
        self._states = SortedList(list)

    def get_number_of_states(self):
        return self._states_number
