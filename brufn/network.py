import networkx as nx
from itertools import chain
from functools import reduce
from sortedcontainers import SortedList
from abc import ABC, abstractmethod
from typing import List, Tuple, Dict
import os
import pickle
from collections import OrderedDict
import itertools
from copy import copy
from itertools import islice

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from brufn.state_factory import StateFactory


def is_t1_best_transition(t1: 'Transition', t2: 'Transition', pf: float = -1):
    if t2 is None:
        return True
    else:
        t1_sdp = t1.get_probability(pf=pf)
        t1_cost = t1.get_cost(pf=pf)
        t2_sdp = t2.get_probability(pf=pf)
        t2_cost = t2.get_cost(pf=pf)

        return t1_sdp > t2_sdp or (t1_sdp == t2_sdp and t1_cost < t2_cost)


class Contact:

    def __init__(self, from_: int, to: int, ts: int, pf: float = 0, identifier: int = -1):
        self._from: int = from_
        self._to: int = to
        self._ts: int = ts
        self.pf: float = pf
        self._id: int = identifier

    def __str__(self):
        return 'send_%d_%d_%d'%(self.from_, self.to, self.ts)

    def to_dict(self) -> dict:
        return {'from':self.from_, 'to': self.to, 'ts': self.ts}

    @property
    def from_(self) -> int:
        return self._from

    @property
    def to(self) -> int:
        return self._to

    @property
    def ts(self) -> int:
        return self._ts

    @property
    def id(self) -> int:
        return self._id


class Route:

    def __init__(self, contacts):
        assert all(contacts[i].to == contacts[i + 1].from_ for i in range(0,len(contacts) - 1)), "It isn't a valid route"
        assert all(contacts[i].ts >= contacts[i + 1].ts for i in range(0, len(contacts) - 1)), "It isn't a valid route"

        self._contacts: tuple[Contact] = tuple(contacts)

    def generate_one_sender_one_route_rules(self, num_of_copies) -> List['Rule']:
        return [OneSenderRule([copies], [self]) for copies in range(1, num_of_copies + 1)]

    def __str__(self):
        return '_'.join([str(c) for c in self.contacts])

    @property
    def contacts(self) -> Tuple[Contact]:
        return self._contacts

    @property
    def contacts_ids(self) -> Tuple[Contact]:
        return tuple(c.id for c in self._contacts)

    @property
    def sender_node(self) -> int:
        return self.contacts[0].from_

    @property
    def receiver_node(self) -> int:
        return self.contacts[-1].to

    def to_softroute(self):
        return SoftRoute(self)

    def hop_count(self):
        return len(self._contacts)


class SoftRoute:

    def __init__(self, route):
        self._contacts_: tuple[int] = route.contacts_ids
        if len(route.contacts) > 0:
            self._sender_node: int = route.sender_node
            self._receiver_node: int = route.receiver_node

    def __str__(self):
        return '_'.join(["i%d"%c for c in self.contacts])

    @property
    def contacts(self) -> Tuple[int]:
        return self._contacts_

    @property
    def contacts_ids(self) -> Tuple[int]:
        return self._contacts_

    @property
    def sender_node(self) -> int:
        return self._sender_node

    @property
    def receiver_node(self) -> int:
        return self._receiver_node

    def __str__(self):
        return '_'.join([str(c) for c in self.contacts])


class Net:

    def __init__(self, num_of_nodes: int, contacts: List[Contact], traffic:dict = None):
        self.num_of_nodes: int = num_of_nodes
        self.contacts: List[Contact] = contacts
        self.traffic: Dict = traffic
        self.num_of_ts: int = max([c.ts for c in self.contacts]) + 1

        self.networkX_graph: List[nx.DiGraph] = [None] * self.num_of_ts

    '''
    Check is a contact is valid, returns true or false
    '''
    @staticmethod
    def contact_is_valid(c, num_of_nodes, pf_is_required=True):
        if type(c) == dict and all(k in c.keys() for k in ['from', 'to', 'ts']) and \
                type(c['from']) == int and \
                type(c['to']) == int and \
                type(c['ts']) == int and \
                0 <= c['from'] < num_of_nodes and \
                0 <= c['to'] < num_of_nodes and \
                c['from'] != c['to'] and \
                c['ts'] >= 0 and \
                (not pf_is_required or
                 ('pf' in c.keys() and type(c['pf']) == float and 0. <= c['pf'] <= 1.)):
            return True
        return False

    '''
    Get Net from file, if traffic required it returns an error if it does not exist.
    returns
        {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}] (,'TRAFFIC':{'from':int,'to':int, 'ts':int})}
    '''
    @staticmethod
    def get_net_from_file(path_to_net, traffic_required=False, contact_pf_required=True):
        input = {}
        file = open(path_to_net, 'r')
        f = exec(file.read(), input)
        file.close()
        # CHECK INPUT FILE HAS THE REQUIRED FIELDS
        if 'NUM_OF_NODES' in input.keys():
            if type(input['NUM_OF_NODES']) != int or input['NUM_OF_NODES'] <= 1:
                TypeError("[ERROR] NUM_OF_NODES must be an integer greater than 1")
            else:
                NUM_OF_NODES = input['NUM_OF_NODES']
        else:
            TypeError("[ERROR] The input network must contain NUM_OF_NODES")

        if 'CONTACTS' in input.keys():
            if type(input['CONTACTS']) != list or len(input['CONTACTS']) < 1:
                TypeError("[ERROR] CONTACTS must be a list with at least 1 element")
            else:
                # Check if each contact is write in the correct way
                for c in input['CONTACTS']:
                    if not Net.contact_is_valid(c, NUM_OF_NODES, contact_pf_required):
                        err = "[ERROR] Contact must described for a dict: {'from':int,'to':int,'ts':int (,'pf':float)} where: \n"
                        err += "\t to, from are different and to,from in [0,NUM_OF_NODES)\n"
                        err += "\t ts >= 0 \n"
                        err += "\t pf in [0.,1.] (pay attention to write the dots!)\n"
                        err += "\t %s does not satisfy the above properties.\n" % str(c)
                        raise TypeError(err)

                contacts = []
                for i in range(len(input['CONTACTS'])):
                    c = input['CONTACTS'][i]
                    contacts.append(Contact(c['from'], c['to'], c['ts'], pf= c['pf'] if 'pf' in c.keys() else None,
                                            identifier=i))


        else:
            raise TypeError("[ERROR] The input network must contain CONTACTS:[{'from':int,'to':int,'ts':int, 'pf':float}]")

        # Traffic is readed if it exists. If traffic_required, it reports an error when traffic does not exist
        if 'TRAFFIC' in input.keys():
            t = input['TRAFFIC']
            if type(t) == dict and all(k in t.keys() for k in ['from', 'to', 'ts']) and \
                    type(t['from']) == int and \
                    type(t['to']) == int and \
                    type(t['ts']) == int and \
                    0 <= t['from'] < NUM_OF_NODES and \
                    0 <= t['to'] < NUM_OF_NODES and \
                    t['from'] != t['to'] \
                    and t['ts'] >= 0:
                return Net(NUM_OF_NODES, contacts,  traffic = t)
            else:
                err = "[ERROR] TRAFFIC must be a dict: {'from':int,'to':int,'ts':int} where: \n"
                err += "\t to, from are different and to,from in [0,NUM_OF_NODES)\n"
                err += "\t ts >= 0\n"
                raise TypeError(err)

        elif traffic_required:
            print("[ERROR] The input network must contain TRAFFIC:{'from':int,'to':int,'ts':int}")
            return {}

        return Net(NUM_OF_NODES, contacts)

    def compute_routes(self, source: int, target: int,  ts: int) -> List[Route]:
        assert 0 <= ts < self.num_of_ts, 'ts must be in [%d, %d)'%(0, self.num_of_ts)

        if self.networkX_graph[ts] is None:
            self.networkX_graph[ts] = nx.DiGraph()
            self.networkX_graph[ts].add_nodes_from(range(self.num_of_nodes))
            for c in self.contacts:
                if c.ts == ts:
                    self.networkX_graph[ts].add_edge(c.from_, c.to, object=c)

        routes = []
        for path in nx.all_simple_paths(self.networkX_graph[ts], source, target):
            routes.append(Route([self.networkX_graph[ts].edges[path[i],path[i+1]]['object'] for i in range(len(path) - 1)]))

        return routes

    def get_dtnsim_cp(self, ts_duration: int, capacity: int) -> str:
        '''
            It builds a DTNSim Contact Plan and return it as string
        :param ts_duration: Duration of any contact in the network
        :param capacity:
        :return: A string with the requierd contact_plan
        '''

        cp = ""
        for c in self.contacts:
            cp += "a contact +%d +%d %d %d %d\n" % (
            c.ts * ts_duration, (c.ts + 1) * ts_duration, c.from_ + 1, c.to + 1, capacity)

        return cp

    def print_dtnsim_cp_to_file(self, ts_duration: int, capacity: int, output_file_path: str):
        '''
            It builds a DTNSim Contact Plan and return it as string
        :param ts_duration: Duration of any contact in the network
        :param capacity:
        :param output_file_path:
        :return: A string with the required contact_plan
        '''
        with open(output_file_path,'w') as f:
            for c in self.contacts:
                c.write("a contact +%d +%d %d %d %d\n" % (c.ts * ts_duration, (c.ts + 1) * ts_duration, c.from_ + 1, c.to + 1, capacity))


    @staticmethod
    def get_net_from_dtnsim_cp(cp_path: str, ts_duration: int) -> 'Net':
        contacts = []
        with open(cp_path) as fin:
            for line in fin:
                if 'a contact' in line:
                    line = line.split(' ')
                    ts_from = int(line[2][1:]) / ts_duration
                    ts_to = int(line[3][1:]) / ts_duration
                    if ts_to - ts_from != 1:
                        err = '''ERROR] It can not translate networks which has contact with
                                more than 1 ts duration. ts_duration was setted to %d''' % ts_duration
                        raise TypeError(err)
                    node_from = int(line[4]) - 1
                    node_to = int(line[5]) - 1
                    contacts.append(Contact(node_from, node_to, int(ts_from)))

        return Net(max([c.from_ for c in contacts] + [c.to for c in contacts]) + 1, contacts)

    def to_dot(self, output_path: str, file_name: str = 'net.dot'):
        '''
        Save file with network's dot representation

        :param output_path: path to destination folder
        :param file_name: name under which the file will be saved
        :return: None
        '''
        with open(os.path.join(output_path, file_name), 'w') as f:
            f.write("digraph G { \n\n")
            f.write("rank=same;\n")
            f.write("ranksep=equally;\n")
            f.write("nodesep=equally;\n")

            for ts in range(self.num_of_ts):
                f.write("\n// TS = %d\n" % ts)
                for n in range(self.num_of_nodes):
                    f.write("%d.%d[label=L%d];\n" % (n, ts, n))

                f.write(str(self.num_of_nodes) + "." + str(ts) + "[shape=box,fontsize=16,label=\"TS " + str(ts) + "\"];\n")

                for n in range(self.num_of_nodes):
                    f.write("%d.%d -> %d.%d[style=\"invis\"];\n" % (n, ts, n + 1, ts))

                for c in self.contacts:
                    if c.ts == ts:
                        f.write("%d.%d -> %d.%d%s\n" % (c.from_, ts, c.to, ts,
                                                           "[color=green,fontcolor=green,penwidth=2]" if "FLAG" in c.__dict__.keys() else ""))

            f.write("\n\n// Ranks\n")
            for n in range(self.num_of_nodes):
                f.write("{ rank = same;")
                for ts in range(self.num_of_ts):
                    f.write(" %d.%d;" % (n, ts))
                f.write("}\n")
            f.write(" \n}")

    def print_to_file(self, output_path: str, file_name: str ='net.py'):
        '''
        This method prints a file at output_path/file_name with a python representation of current network.

        :param output_path:
        :param file_name:
        :return: None
        '''
        f = open(output_path + (lambda s: "/" if s[-1] != '/' else '')(output_path) + file_name, 'w')
        f.write("NUM_OF_NODES = %d\n" % self.num_of_nodes)
        for c in self.contacts:
            f.write("CONTACTS = %s \n" % c.to_dict())
        if self.traffic is not None:
            # Traffic is optional
            f.write("TRAFFIC = %s\n" % self.traffic)

        f.close()

    def get_contact_by_id(self, id):
        for c in self.contacts:
            if c.id == id:
                return c


class State:

    def __init__(self, states: List[int], ts, codified_param=None, f_cmp_transition = is_t1_best_transition):
        '''

        :param states: A list[int], states[i] account for the number of copies carried by node i.
        :param ts: Int, timestamp
        :param codified_param: A integer with the number of copies in the model, in order to numerate all states.
        '''

        if not all([0 <= b for b in states]):
            raise TypeError("The number of bundles must be positive")

        self._states = tuple(states)
        self._ts: int = ts
        self._codified_param: int = codified_param
        if codified_param is not None:
            self._id = State.get_identifier(self._states, self._ts, codified_param)

        self._transitions: List['Transition'] = []
        self._max_success_pr: Dict[str, float] = None # '-1' is used when no probability range is given
        self._max_success_transition: Dict[str, 'Transition'] = None # '-1' is used when no probability range is given
        self.is_best_transition = f_cmp_transition

    def add_transition(self, t):
        self._transitions.append(t)

    def get_carrier_nodes(self) -> List[int]:
        return [x for x in range(self.num_of_nodes) if self._states[x] > 0]

    def num_of_carrying_copies(self, node: int) -> int:
        return self._states[node]

    def gen_previous_state(self, rules: List['Rule'], state_factory: 'StateFactory') -> 'State':
        previous = [x for x in self._states]
        for rule in rules:
            for sender, receiver, copies in zip(rule.sender_node, rule.receiver_node, rule.copies):
                previous[sender] += copies
                previous[receiver] -= copies

        return state_factory.create_state(previous, self.ts - 1)

    def to_softstate(self, solved_transition_required=True):
        return SoftState(self, solved_transition_required)

    @property
    def num_of_nodes(self) -> int:
        return len(self._states)

    @property
    def ts(self) -> int:
        return self._ts

    @property
    def states(self) -> int:
        return self._states

    @property
    def id(self) -> int:
        return self._id

    @property
    def codified_param(self) -> int:
        return self._codified_param

    @property
    def transitions(self) -> Tuple['Transition']:
        return tuple(self._transitions)

    def max_success_transition(self, pf: float = -1) -> 'Transition':
        return self._max_success_transition[str(pf)]

    def clean_success_probability(self):
        self._max_success_transition = None
        self._max_success_pr = None

    def compute_probability(self, target: int) -> float:
        if len(self._transitions) == 0:
            self._max_success_pr = {'-1': int(self._states[target] > 0)}
            self._max_success_transition = {'-1': None}
        else:
            max_success_transition:'Transition' = self._transitions[0]
            max_success_pr: float = max_success_transition.compute_probability()
            for t in self._transitions:
                pr = t.compute_probability()
                #Update if
                if self.is_best_transition(t, max_success_transition):
                    max_success_transition = t
                    max_success_pr = pr
            self._max_success_transition = {'-1': max_success_transition}
            self._max_success_pr = {'-1': max_success_pr}

        return self._max_success_pr['-1']

    def compute_probability_rng(self, target: int, pr_rng: List[float], next_level=None, state_factory=None) -> Dict[str, float]:
        if len(self._transitions) == 0:
            pr_value = int(self._states[target] > 0)
            self._max_success_pr = dict([(str(pr), pr_value) for pr in pr_rng])
            self._max_success_transition = dict([(str(pr), None) for pr in pr_rng])
        else:

            self._max_success_pr = dict((pr, -1) for pr in pr_rng)
            self._max_success_transition = dict((str(pr), None) for pr in pr_rng)

            while len(self._transitions) > 0:
                t = self._transitions.pop()
                t.compute_changes(next_level, state_factory)
                t.compute_probability_rng(pr_rng)
                for pf in pr_rng:
                    if self.is_best_transition(t, self.max_success_transition(pf=pf), pf=pf):
                        self._max_success_pr[str(pf)] = t.get_probability(pf=pf)
                        self._max_success_transition[str(pf)] = t

        return self._max_success_pr

    def get_probability(self, pf: float = -1):
        assert self._max_success_pr is not None, " State:get_probability was called but it has not been computed yet."
        return self._max_success_pr[str(pf)] #It coud be a dict or a float

    def __eq__(self, other):
        if isinstance(other, State):
            return self._id == other._id
        raise TypeError('Comparing State with %s'(type(other)))

    def __lt__(self, other):
        if isinstance(other, State):
            return self._id < other._id
        raise TypeError('Comparing State with %s'(type(other)))

    def __str__(self):
        return "State: ts: %d - %s"%(self._ts, self._states)

    @staticmethod
    def get_identifier(copies: List[int], ts: int, num_of_copies: int)-> int:
        l = [ts * (num_of_copies + 1) ** len(copies)]
        l += [copies[i] * (num_of_copies + 1) ** (len(copies) - i - 1) for i in range(len(copies))]
        return sum(l)

    @staticmethod
    def get_state_repr_by_identifier(id:int, ts_number: int, nodes_number: int, num_of_copies: int)-> str:

        for ts in range(ts_number):
            it = Index([0] * nodes_number, [num_of_copies+1] * nodes_number)
            while not it.finish():
                if State.get_identifier(it.values, ts, num_of_copies) == id:
                    return str(it.values) + '- %d'%ts
                it.next()



class SoftState:

    def __init__(self, state: State, solved_transition_required):
        '''

        :param states: A list[int], states[i] account for the number of copies carried by node i.
        :param ts: Int, timestamp
        :param codified_param: A integer with the number of copies in the model, in order to numerate all states.
        '''

        self._states_: Tuple[int] = state.states
        self._ts_: int = state.ts
        self._id_ = state.id

        if solved_transition_required:
            if state._max_success_pr is None:
                raise TypeError("SoftState:__init__ solved_transition_required but it has not been solved yet.")

            #self._transitions_: List[SoftTransition] = [t.to_SoftTransition() for t in state.transitions]
            self._max_success_pr_ = state._max_success_pr
            self._max_success_transition_ = dict()
            self._transitions_ = dict()
            for pf in state._max_success_transition.keys():
                self._max_success_transition_[pf] = (lambda x: x.to_SoftTransition(pf=pf) if x is not None else None) (state._max_success_transition[pf])
                #if __debug__:
                #    self._transitions_[pf] = tuple(t.to_SoftTransition(pf) for t in state.transitions) # It is very expensive to save all transitions!

    @property
    def transitions(self):
        return self._transitions_

    def pickle_dump(self, path_to_folder):
        output_path = os.path.join(path_to_folder,'id=%d.pickle'%(self._id_))
        output_file = open(output_path, 'wb')
        pickle.dump(self, output_file)
        output_file.close()

    @staticmethod
    def pickle_load(path_to_folder:str, id:int) -> 'State':
        input_path = os.path.join(path_to_folder,'id=%d.pickle'%(id))
        input_file = open(input_path, 'rb')
        state = pickle.load(input_file)
        input_file.close()
        return state

    @property
    def num_of_nodes(self):
        return len(self._states_)

    @property
    def ts(self):
        return self._ts_

    @property
    def states(self):
        return self._states_

    @property
    def id(self):
        return self._id_

    def max_success_transition(self, pf: float = -1) -> 'Transition':
        return self._max_success_transition_[str(pf)]

    def get_probability(self, pf:float = -1):
        return self._max_success_pr_[str(pf)]

    def to_state(self):
        s = State(self._states_, self._ts_)
        s._id = self._id_
        s._max_success_pf = self._max_success_pr_
        return s

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, SoftState) or isinstance(other, State):
            return self._id_ == other.id
        raise TypeError('Comparing SoftState with %s'%(type(other)))

    def __lt__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, SoftState) or isinstance(other, State):
            return self._id_ < other.id
        raise TypeError('Comparing SoftState with %s'%(type(other)))


class Rule(ABC):

    def __init__(self, num_of_copies, route):
        assert (isinstance(num_of_copies, int) and not isinstance(route, list)) or len(num_of_copies) == len(route)
        self._copies = num_of_copies
        self._route = route
        self._copies_sum = num_of_copies if isinstance(num_of_copies, int) else sum(num_of_copies)

    @property
    @abstractmethod
    def sender_node(self) -> List[int]:
        pass

    @property
    @abstractmethod
    def receiver_node(self) -> List[int]:
        pass

    @property
    @abstractmethod
    def copies(self) -> List[int]:
        pass

    def get_contacts(self):
        return self._route.contacts

    def get_contacts_ids(self):
        return (c.id for c in self.get_contacts())

    @property
    def route(self):
        return self._route

    def to_json(self) -> str:
        return str(self)

    @abstractmethod
    def to_dtnsim_rule(self) -> List[str]:
        pass

    @abstractmethod
    def to_SoftRule(self):
        raise TypeError('This method is abstract, it must not be called')
        pass

    @property
    def copies_sum(self):
        return self._copies_sum

    def hop_count(self):
        return sum(route.hop_count() for route in self.route)


class SoftRule:
    def __init__(self, rule: Rule):
        self._copies_: Tuple[int] = rule.copies
        self._route_: SoftRoute = rule.route.to_softroute()

    def to_tuple(self) -> Tuple[int,Tuple[int]]:
        res = tuple( zip(self._copies_, self._route_) )
        return [(x[0], x[1].contacts) for x in res]

    @abstractmethod
    def to_dtnsim_rule(self) -> List[str]:
        pass


class OneSenderRule(Rule):
    def __init__(self, num_of_copies: List[int], routes:List[Route]):
        assert(all(r.sender_node == routes[0].sender_node for r in routes))
        super(OneSenderRule, self).__init__(num_of_copies, routes)

        assert len(set(self.sender_node)) == 1, 'OneSenderRule must have rules from more only 1 sender node'

    @property
    def sender_node(self) -> List[int]:
        return [self._route[0].sender_node] * len(self.route)

    @property
    def receiver_node(self) -> List[int]:
        return [self._route[0].receiver_node] * len(self.route)

    @property
    def copies(self) -> List[int]:
        return self._copies

    def to_dtnsim_rule(self) -> List[str]:
        assert len(self._copies) == len(self._route)

        one_sender_rules_splited = []
        for copy, route in zip(self._copies, self._route):
            dtnsim_rule = OrderedDict()
            dtnsim_rule['copies'] = copy
            dtnsim_rule['name'] = 'send%d_%d-toward:%s'%(route.sender_node, route.receiver_node, tuple(route.contacts_ids))
            dtnsim_rule['contact_ids'] = [c + 1 for c in route.contacts_ids]
            dtnsim_rule['source_node'] = route.sender_node + 1
            one_sender_rules_splited.append(dtnsim_rule)

        return one_sender_rules_splited

    def __str__(self):
        return "Y".join(["%d_%s"%(copy, str(route)) for copy, route in zip(self.copies, self._route)])

    def to_SoftRule(self):
        return SoftOneSenderRule(self)

    def get_contacts(self):
        return list(chain.from_iterable([r.contacts for r in self._route]))


class SoftOneSenderRule(SoftRule):
    def __init__(self, rule: OneSenderRule):
        #super(SoftOneSenderRule, self).__init__(rule)
        self._route_ = tuple([r.to_softroute() for r in rule.route])
        self._copies_: tuple(int) = tuple(rule.copies)
        self._sender = rule.sender_node[0]
        self._receiver = rule.receiver_node[0]

    def __str__(self):
        return "Y".join(["%d_%s"%(copy, str(route)) for copy, route in zip(self._copies_, self._route_)])

    def to_dtnsim_rule(self) -> List[str]:
        assert len(self._copies_) == len(self._route_)

        one_sender_rules_splited = []
        for copy, route in zip(self._copies_, self._route_):
            dtnsim_rule = OrderedDict()
            dtnsim_rule['copies'] = copy
            dtnsim_rule['name'] = 'send%d_%d-toward:%s'%(self._sender, self._receiver,route.contacts)
            dtnsim_rule['contact_ids'] = [c + 1 for c in route.contacts]
            dtnsim_rule['source_node'] = self._sender + 1
            one_sender_rules_splited.append(dtnsim_rule)

        return one_sender_rules_splited


class ManySenderRule(Rule):
    def __init__(self, num_of_copies, routes, next_copies = 0):
        if not (isinstance(num_of_copies, list) and isinstance(routes, list)):
            raise TypeError('ManySenderRule must be greated with more than one sender node')

        super(ManySenderRule, self).__init__(num_of_copies, routes)
        self._next_copies = next_copies
        assert len(set(self.sender_node)) > 1, 'ManySenderRule must have rules from more than 1 sender node'

    @property
    def sender_node(self):
        return [r.sender_node for r in self._route]

    @property
    def receiver_node(self):
        return [self._route[0].receiver_node] * len(self._route)

    @property
    def copies(self):
        return self._copies

    def get_contacts(self):
        return set(chain.from_iterable([r.contacts for r in self._route]))

    def __str__(self):
        return "Y".join(["%d_%s"%(copy, str(route)) for copy, route in zip(self.copies, self._route)])

    def to_dtnsim_rule(self):
        one_sender_rules = []
        for copy, route in zip(self.copies, self.route):
            osrule = OneSenderRule(copy, [route])
            one_sender_rules.extend(osrule.to_dtnsim_rule())

        return one_sender_rules

    def to_SoftRule(self):
        return SoftManySenderRule(self)


class SoftManySenderRule(SoftRule):
    def __init__(self, rule:ManySenderRule):
        #super(SoftManySenderRule, self).__init__(rule)
        self._route_ = tuple([r.to_softroute() for r in rule.route])
        self._copies_:tuple(int) = tuple(rule.copies)
        self._next_copies_ = rule._next_copies


    def __str__(self):
        return "Y".join(["%d_%s"%(copy, str(route)) for copy, route in zip(self._copies_, self._route_)])

    def to_dtnsim_rule(self):
        one_sender_rules = []
        for copy, route in zip(self._copies_, self._route_):
            osrule = OneSenderRule([copy], [route])
            one_sender_rules += osrule.to_dtnsim_rule()

        assert len(set([r.receiver_node for r in self._route_])) == 1
        receiver_node = self._route_[0].receiver_node
        if self._next_copies_ > 0:
            one_sender_rules.append(NextRule(receiver_node, self._next_copies_).to_dtnsim_rule()[0])
        return one_sender_rules

class NextRule(Rule):

    def __init__(self, node: int, num_of_copies: int):
        super(NextRule, self).__init__(num_of_copies, Route([]))
        self._node = node

    @property
    def sender_node(self) -> List[int]:
        return [self._node]

    @property
    def receiver_node(self) -> List[int]:
        return [self._node]

    @property
    def copies(self) -> List[int]:
        return [self._copies]

    def to_dtnsim_rule(self) -> List[str]:
        dtnsim_rule = OrderedDict()
        dtnsim_rule['copies'] = self.copies[0]
        dtnsim_rule['name'] = 'next'
        dtnsim_rule['contact_ids'] = []
        dtnsim_rule['source_node'] = self._node + 1

        return [dtnsim_rule]

    def __str__(self):
        return "Next"

    def to_SoftRule(self):
        return SoftNextRule(self)

    def hop_count(self):
        return 0


class SoftNextRule(SoftRule):

    def __init__(self, rule: NextRule):
        super(SoftNextRule, self).__init__(rule)
        self._node_ = rule._node

    def __str__(self):
        return "Next"

    def to_dtnsim_rule(self) -> List[str]:
        dtnsim_rule = OrderedDict()
        dtnsim_rule['copies'] = self._copies_[0]
        dtnsim_rule['name'] = 'next'
        dtnsim_rule['contact_ids'] = []
        dtnsim_rule['source_node'] = self._node_ + 1

        return [dtnsim_rule]


class ChangeCase:
    '''
    A change case is described by contacts and when it fails or work:
        _case[i] stored 0 if _contacts[i] is considered to fail in this case or
        1 otherwise.
    '''

    def __init__(self, contacts, case):
        self._contacts: Tuple[Contact] = contacts
        self._case: Tuple[int] = case

        self._pr_result: Dict[str, float] = None #Store the probability of this ChangeCase happen. Dict or float

    def get_probability(self, pf:float = -1):
        if self._pr_result is None:
            raise TypeError("ChangeCase:get_probability was called but it has not been computed yet.")

        return self._pr_result[str(pf)]

    def compute_probability(self) -> float:
        prs = [1 - self._contacts[i].pf if self._case[i] else self._contacts[i].pf for i in range(len(self._contacts))]
        self._pr_result = {'-1': reduce(lambda x, y: x * y, prs, 1.)}

        return self._pr_result['-1']

    def compute_probability_rng(self, pr_rng) -> Dict:
        '''
        :param pr_rng: A range in which each individual value is the failure probability for all links
        '''
        n_of_working_links = sum(self._case)
        self._pr_result = dict([(str(pf), pf ** (len(self._case) - n_of_working_links) * (1-pf) ** n_of_working_links) for pf in pr_rng])

        return self._pr_result

    def get_failure_links(self) -> Tuple[Contact]:
        return tuple(self._contacts[i] for i in range(len(self._contacts)) if not self._case[i])

    def to_SoftChangeCase(self):
        return SoftChangeCase(self)


class SoftChangeCase:
    def __init__(self, change_case):
        self._contacts_:List[int] = tuple([c.id for c in change_case._contacts])
        self._case_:List[int] = change_case._case
        self._pr_result_:float = copy(change_case._pr_result)

    def get_probability(self):
        return self._pr_result_


class Change:
    '''
    A change is a possible update in a transition. It is described by the update,
    it means which state will be gotten and for a list of cases that stored all cases in which
    this change is performed. The Change probability is computed by summing all cases probability
    '''

    def __init__(self, to_state):
        self._cases: List[ChangeCase] = []
        self._to_state: State = to_state

        self._pr_result = None

    def add_case(self, case):
        self._cases.append(case)

    def get_probability(self, pf: float = -1):
        if self._pr_result is None:
            raise TypeError('Change:get_probability was called but it has not been computed yet')
        return self._pr_result[str(pf)]

    def compute_probability(self) -> float:
        self._pr_result = {'-1': sum([case.compute_probability() for case in self._cases])}
        return self._pr_result ['-1']

    def compute_probability_rng(self, pr_rng) -> Dict:
        '''
        :param pr_rng:
        :return:
        '''
        self._pr_result = dict([(str(pf), 0) for pf in pr_rng])
        for cc in self._cases:
            cc_pr = cc.compute_probability_rng(pr_rng)
            for pf in pr_rng:
                self._pr_result[str(pf)] += cc_pr[str(pf)]

        return self._pr_result

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, Change):
            return self.id == other.id
        return False

    @property
    def id(self):
        return self._to_state.id

    @property
    def to_state(self):
        return self._to_state

    @property
    def cases(self):
        return tuple(self._cases)

    def to_SoftChange(self):
        return SoftChange(self)


class SoftChange:
    def __init__(self, change):
        #self._cases_: int = [cc.to_SoftChangeCase() for cc in change.cases]
        self._to_state_: int = change.to_state.id
        self._pr_result_: Dict[str, float] = change._pr_result #it could be float or dict

    def get_probability(self, pf: float = -1):
        return self._pr_result_[str(pf)]

    @property
    def to_state(self):
        return self._to_state_


class Transition:
    '''
    A transition is a tuple of Rules
    '''

    def __init__(self, from_, to, rules):
        self._from: SoftState = from_.to_softstate(solved_transition_required=False)
        self._to: State = to
        self._rules: Tuple[Rule] = tuple(rules)
        self._changes_list: SortedList[Change] = SortedList(key=lambda change: change.id)
        self._cost: Dict[str, int] = None
        self._pr_result: Dict[str, float] = None

    def get_cost(self, pf:float = -1) -> int:
        if self._cost is None:
            raise TypeError('Transition:get_cost was called but it has not been computed yet')

        return self._cost[str(pf)]

    def __str__(self):
        rule = '__r'.join([str(x) for x in self._rules]).replace(' ','')
        guard = ' & '.join(['(n%d=%d)'%(i, self.from_.states[i]) for i in range(self.from_.num_of_nodes)] + ['(ts = %d)'%self.from_.ts])

        changes = []
        acum_pr = 0.
        for change in self._changes_list:
            change_str = '%f: '%(change.get_probability())
            change_str += ' & '.join(["(n%d'=%d)"%(i, change.to_state.states[i]) for i in range(change.to_state.num_of_nodes)] + ["(ts' = %d)"% change.to_state.ts])
            changes.append(change_str)
            acum_pr += change.get_probability()

        if acum_pr < 1.:
            copies = sum(self._from.states)
            nodes = len(self._from.states)
            changes.append('%f: '%(1-acum_pr) + ' & '.join(["(n%d'=%d)"%(i, copies) for i in range(change.to_state.num_of_nodes)] + ["(ts' = 0)"]))


        return '[r__%s] %s -> %s;'%(rule, guard, '+'.join(changes))

    def _add_change_case(self, change_case, state_factory):
        to_state = self._compute_tostate(change_case, state_factory)
        if to_state is not None:
            change = Change(to_state)
            try:
                index = self._changes_list.index(change)
                change = self._changes_list[index]
            except ValueError:
                self._changes_list.add(change)

            change.add_case(change_case)

    def _compute_tostate(self, change_case, state_factory):
        '''
        Returns the state that will be reached if the change_case happen

        :param change_case:
        :return: The State that will be reached if the change_case happen
        '''

        carrier_nodes = self.to.get_carrier_nodes()
        to_status = [i for i in self.from_.states]
        failure_set = [c.id for c in change_case.get_failure_links()]

        for i in range(len(carrier_nodes)):
            rule = self._rules[i]
            if isinstance(rule, OneSenderRule) or isinstance(rule, ManySenderRule):
                for route, copies in zip(rule.route, rule.copies):
                    for c in route.contacts:
                        if c.id in failure_set:
                            break
                        else:
                            to_status[c.from_] -= copies
                            to_status[c.to] += copies

        return state_factory.create_state(to_status, self.from_.ts + 1, allow_creation=False)

    def compute_changes(self, reachable_states, state_factory):
        '''
        Compute all reachable states by considering all possible failures

        :param reachable_states: A list with states from which successful states can be reached
        :param state_factory:
        :return: None. Its save the information inside transition object
        '''

        contacts_used = []
        contacts_used_ids = []
        for rule in self._rules:
            for c in rule.get_contacts():
                if c.id not in contacts_used_ids:
                    contacts_used_ids.append(c.id)
                    contacts_used.append(c)

        #generate all diferent cases
        change_contacts = tuple(contacts_used)
        generator = Index([0] * len(contacts_used), [2] * len(contacts_used))
        while not generator.finish():
            case = ChangeCase(change_contacts, tuple(generator.values))
            self._add_change_case(case, state_factory)
            generator.next()

    def get_probability(self, pf = -1):
        if self._pr_result is None:
            raise TypeError('Transition:get_probability was called but it has not been computed yet')
        return self._pr_result[str(pf)]

    def compute_probability(self) -> float:
        cost = sum(rule.hop_count() for rule in self._rules)
        pr_result = 0.
        for change in self._changes_list:
            pr_result += change.compute_probability() * change.to_state.get_probability()
            if change.to_state.max_success_transition() is not None:
                cost += change.compute_probability() * change.to_state.max_success_transition().get_cost()

        self._cost = {'-1': cost}
        self._pr_result = {'-1': pr_result}
        return pr_result

    def compute_probability_rng(self, pr_rng) -> Dict:
        t_cost = sum(rule.hop_count() for rule in self._rules)
        self._pr_result = dict([(str(pf),0.) for pf in pr_rng])
        self._cost = dict([(str(pf), t_cost) for pf in pr_rng])

        for change in self._changes_list:
            change_pf_rng = change.compute_probability_rng(pr_rng)
            for pf in pr_rng:
                self._pr_result[str(pf)] += change_pf_rng[str(pf)] * change.to_state.get_probability(pf=pf)
                mst = change.to_state.max_success_transition(pf=pf)
                if mst is not None:
                    self._cost[str(pf)] += change_pf_rng[str(pf)] * mst.get_cost(pf)

        return self._pr_result

    def to_dtnsim_action(self) -> List[str]:
        return list(chain.from_iterable([rule.to_dtnsim_rule() for rule in self.rule]))

    @property
    def from_(self):
        return self._from

    @property
    def to(self):
        return self._to

    @property
    def rule(self) -> Tuple[Rule]:
        return self._rules

    def to_SoftTransition(self, pf=None):
        return SoftTransition(self, pf=pf)


class SoftTransition:

    def __init__(self, transition: Transition, pf=None):
        self._from_: int = transition.from_.id
        self._to_: int = transition.to.id
        self._rules_: Tuple[SoftRule] = tuple([r.to_SoftRule() for r in transition.rule])
        self._changes_list_: tuple[SoftChange] = tuple([r.to_SoftChange() for r in transition._changes_list])
        self._cost_ = copy(transition._cost)
        self._pr_result = copy(transition._pr_result)


    @property
    def rule(self) -> Tuple[SoftRule]:
        return self._rules_

    @property
    def change_list(self) -> Tuple[SoftChange]:
       return self._changes_list_

    def to_tuple_rules(self):
        l = []
        for rule in self._rules_:
            if type(rule) != SoftNextRule:
                l.append(rule.to_tuple())

        return tuple(itertools.chain.from_iterable(l))

    def to_dtnsim_action(self) -> List[str]:
        return list(chain.from_iterable([rule.to_dtnsim_rule() for rule in self.rule]))

    def get_cost(self, pf: float = -1):
        return self._cost_[str(pf)]


class Index:

    # Iterator indexes for all i in self.states: min <= i < max
    def __init__(self, min_, max_):
        self._values = [x for x in min_]
        self._max = max_
        self._end = False

    def next(self):
        assert not self._end

        for i in range(len(self.values) - 1, -1, -1):
            self._values[i] = (self._values[i] + 1) % self._max[i]
            if self._values[i] > 0:
                break
        self._end = all([i == 0 for i in self._values])

    def finish(self):
        return self._end

    @property
    def values(self):
        return tuple(self._values)