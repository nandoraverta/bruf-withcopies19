import unittest
from brufn.network import *
import random
from brufn.MemEffBRUF19 import BRUF
import itertools
import os

class Testing(unittest.TestCase):

    '''
    AUXILIARY METHODS
    '''
    def gen_net1(self, num_of_nodes) -> Net:
        '''
        it generates a net in which i is linked with i+1. Besides, node i < num_of_nodes-2 has a direct contact to num_of_nodes-1
        :return:
        '''
        contacts = [Contact(x, x + 1, 0, identifier=x) for x in range(num_of_nodes - 1)]
        contacts += [Contact(x, num_of_nodes - 1, 0, identifier=x + len(contacts)) for x in range(num_of_nodes - 2)]

        return Net(num_of_nodes, contacts)

    def one_sender_rules_aux_method(self, copies):
        net = self.gen_net1(6)

        state = State([0] * 5 + [copies], 0, codified_param=3)
        bruf = BRUF(net, [0], 5, 3, [], '')
        self.assertEqual(state.num_of_carrying_copies(5), copies)
        hardcoded_routes = [
            [(0, 1, 2, 3, 4), (5,), (0, 6), (0, 1, 7), (0, 1, 2, 8)],  # from Node 0
            [(1, 2, 3, 4), (6,), (1, 7), (1, 2, 8)],  # from Node 1
            [(2, 3, 4), (7,), (2, 8)],  # from Node 2
            [(3, 4), (8,)],  # from Node 3
            [(4,)]  # from Node 4
        ]

        required_rules = []
        for node in range(net.num_of_nodes - 1):
            hardcoded_rules_by_node = [(c, hardcoded_routes[node][i]) for i in range(len(hardcoded_routes[node])) for c in range(1, copies + 1)]
            for c in range(1, copies + 1):
                required_rules.append([])
                for rule in itertools.combinations(hardcoded_rules_by_node, c):
                    if sum([x[0] for x in rule]) <= copies:
                        required_rules[node].append(rule)

        for sender in range(5):
            rules_copies_minus_1, rules = bruf.compute_one_sender_rules(sender, 5, 0, state.num_of_carrying_copies(5))
            # check number of rules
            self.assertEqual(len(required_rules[sender]), len(rules), 'Fail OneSenderRule with %d copies, for sender = %d'%(copies,sender))
            self.assertEqual(len(list(filter(lambda rule: sum([x[0] for x in rule]) < (state.num_of_carrying_copies(5)), required_rules[sender]))), rules_copies_minus_1)
            # check each rule
            for hr in required_rules[sender]:
                exist_rule = False
                for rule in rules:
                    if len(rule.route) == len(hr) and all(any(rule.route[i].contacts_ids == irh[1] and rule.copies[i] == irh[0]
                                                    for i in range(len(rule.route))) for irh in hr):
                        exist_rule = True
                        break

                self.assertTrue(exist_rule,
                                "Fail OneSenderRule with %d copies from Sender %d: %s is not listed" % (
                                copies, sender, str(hr)))

            for r in rules:
                self.assertTrue(isinstance(r, OneSenderRule), 'Rules must be instance of OneSenderRule')
                self.assertEqual(r.sender_node, [sender] * len(r.route), 'Sender node must be %d' % sender)
                self.assertEqual(r.receiver_node, [5] * len(r.route), 'Receiver node must be 5')

    def many_sender_rules_aux_method(self, copies):
        net = self.gen_net1(6)

        state = State([0] * 5 + [copies], 0, codified_param=3)
        bruf = BRUF(net, [0], 5, copies, [], '')
        self.assertEqual(state.num_of_carrying_copies(5), copies)
        hardcoded_routes = [
            [(0, 1, 2, 3, 4), (5,), (0, 6), (0, 1, 7), (0, 1, 2, 8)],  # from Node 0
            [(1, 2, 3, 4), (6,), (1, 7), (1, 2, 8)],  # from Node 1
            [(2, 3, 4), (7,), (2, 8)],  # from Node 2
            [(3, 4), (8,)],  # from Node 3
            [(4,)]  # from Node 4
        ]

        hc_one_sender_rules_by_node = []
        for node in range(net.num_of_nodes - 1):
            hardcoded_rules_by_node = [(c, hardcoded_routes[node][i]) for i in range(len(hardcoded_routes[node])) for c
                                       in range(1, copies + 1)]
            for c in range(1, copies + 1):
                hc_one_sender_rules_by_node.append([])
                for rule in itertools.combinations(hardcoded_rules_by_node, c):
                    if sum([x[0] for x in rule]) <= copies:
                        hc_one_sender_rules_by_node[node].append(rule)

        required_rules = []
        required_rules_senders = []
        generators = [(node, route) for node in range(net.num_of_nodes - 1) for route in
                      range(len(hc_one_sender_rules_by_node[node]) + 1)]
        for intended_rule in itertools.combinations(generators, net.num_of_nodes - 1):
            sender_nodes = [x[0] for x in intended_rule]
            sender_nodes_set = set(sender_nodes)
            if len(sender_nodes_set) > 1 and len(sender_nodes_set) == len(sender_nodes):  # Only allow one rule by node
                rule = [hc_one_sender_rules_by_node[r[0]][r[1] - 1] for r in intended_rule if r[1] > 0]
                if len(rule) > 1 and 0 < sum([r[0] for r in itertools.chain.from_iterable(rule)]) <= copies:
                    # Append only valid ManySenderRules. It means, more than 1 sender node which send proper number of copies
                    required_rules.append(list(itertools.chain.from_iterable(rule)))
                    required_rules_senders.append(set([x[0] for x in intended_rule if x[1] > 0]))

        '''
        Generate rules using the test target code
        '''
        one_sender_rules = []
        for sender in range(net.num_of_nodes - 1):
            rules_copies_minus_1, rules = bruf.compute_one_sender_rules(sender, 5, 0, state.num_of_carrying_copies(5))
            one_sender_rules.append(rules[:rules_copies_minus_1])

        one_sender_rules = [[x for x in node_rules] + [NextRule(-1, 0)] for node_rules in one_sender_rules]

        many_sender_rules = bruf.compute_many_sender_rules(one_sender_rules, copies)

        # Check number of rules
        self.assertEqual(len(required_rules), len(many_sender_rules), 'Fail ManySenderRules with %d copies' % (copies))

        # Check that each required rule is listed
        for hr, sender_nodes in zip(required_rules, required_rules_senders):
            exist_rule = False
            for rule in many_sender_rules:
                if len(rule.route) == len(hr) and all(
                        any(rule.route[i].contacts_ids == irh[1] and rule.copies[i] == irh[0]
                            for i in range(len(rule.route))) for irh in hr):
                    exist_rule = True
                    self.assertTrue(isinstance(rule, ManySenderRule),
                                    'Rules must be instance of ManySenderRule. An instance of %s has been found' % type(
                                        rule))
                    self.assertTrue(all(x in rule.sender_node for x in sender_nodes), 'Sender node differs from expected: Expected %s in %s Rule: %s'%(sender_nodes, rule.sender_node, str(rule)))
                    self.assertEqual([5] * len(rule.route), rule.receiver_node, 'Receiver node must be 5. Rule: %s'%(str(rule)))
                    break

            self.assertTrue(exist_rule, "Fail OneSenderRule with %d copies: %s is not listed"%(copies, str(hr)))

    '''
    TEST CASES
    '''

    def test_states_identifier(self):
        counter = 0
        for ts in range(10):
            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        s = State([i,j,k], ts, codified_param=2)
                        self.assertEqual(s.id, counter, "Should be %d"%counter)
                        counter += 1

    def test_getting_all_paths1(self):
        net = Net.get_net_from_file('c1.py', contact_pf_required=False)
        routes_0_1_0 = net.compute_routes(0, 1, 0)
        routes_0_2_0 = net.compute_routes(0, 2, 0)
        routes_1_2_0 = net.compute_routes(1, 2, 0)
        routes_1_2_1 = net.compute_routes(1, 2, 1)
        routes_0_2_2 = net.compute_routes(0, 2, 2)
        routes_1_2_2 = net.compute_routes(1, 2, 2)

        self.assertEqual(len(routes_0_1_0), 1, 'There must be 1 path from 0 to 1 at ts 0 but %d'%len(routes_0_1_0))
        self.assertEqual([x.id for x in routes_0_1_0[0].contacts], [0], 'send toward contact 0')

        self.assertEqual(len(routes_0_2_0), 1, 'There must be 1 path from 0 to 2 at ts 0 but %d'%len(routes_0_2_0))
        self.assertEqual([x.id for x in routes_0_2_0[0].contacts], [0,1], 'send toward contact 1')

        self.assertEqual(len(routes_1_2_0), 1, 'There must be 1 path from 1 to 2 at ts 0 but %d'%len(routes_1_2_0))
        self.assertEqual([x.id for x in routes_1_2_0[0].contacts], [1], 'send toward contact 1')

        self.assertEqual(len(routes_1_2_1), 1, 'There must be 1 path from 1 to 2 at ts 1 but %d'%len(routes_1_2_1))
        self.assertEqual([x.id for x in routes_1_2_1[0].contacts], [2], 'send toward contact 1')

        self.assertEqual(len(routes_0_2_2), 1, 'There must be 1 path from 0 to 2 at ts 2 but %d'%len(routes_0_2_2))
        self.assertEqual([x.id for x in routes_0_2_2[0].contacts], [3], 'send toward contact 1')

        self.assertEqual(len(routes_1_2_2), 1, 'There must be 1 path from 1 to 2 at ts 2 but %d'%len(routes_1_2_2))
        self.assertEqual([x.id for x in routes_1_2_2[0].contacts], [4], 'send toward contact 1')

    def test_getting_all_paths2(self):
        net = self.gen_net1(6)
        routes_0_5_0 = net.compute_routes(0, net.num_of_nodes - 1, 0)

        self.assertEqual(len(routes_0_5_0), 5, 'There must be 5 path from 0 to 5 at ts 0 but %d' % len(routes_0_5_0))

        hardcoded_routes = [[0,1,2,3,4], [5], [0, 6], [0,1,7], [0,1,2,8]]
        computed_routes = [[x.id for x in r.contacts] for r in routes_0_5_0]

        self.assertTrue(all(x in computed_routes for x in hardcoded_routes), 'Expected %s get %s'%(hardcoded_routes, computed_routes))

    def test_getting_all_paths3(self):
        net = self.gen_net1(6)
        routes_1_5_0 = net.compute_routes(1, net.num_of_nodes - 1, 0)

        self.assertEqual(len(routes_1_5_0), 4, 'There must be 5 path from 0 to 5 at ts 0 but %d' % len(routes_1_5_0))

        hardcoded_routes = [[1, 2, 3, 4], [6], [1, 7], [1,2,8]]
        computed_routes = [[x.id for x in r.contacts] for r in routes_1_5_0]

        self.assertTrue(all(x in computed_routes for x in hardcoded_routes), 'Expected %s get %s'%(hardcoded_routes, computed_routes))

    def test_getting_all_paths_nonloop(self):
        contacts = [Contact(0, 1, 0, identifier=0), Contact(1, 2, 0, identifier=1), Contact(1, 0, 0, identifier=2), Contact(0, 2, 0, identifier=3)]

        net = Net(3, contacts)

        routes_1_5_0 = net.compute_routes(0, 2, 0)

        self.assertEqual(len(routes_1_5_0), 2, 'There must be 5 path from 0 to 5 at ts 0 but %d' % len(routes_1_5_0))

        hardcoded_routes = [[3],[0,1]]
        computed_routes = [[x.id for x in r.contacts] for r in routes_1_5_0]

        self.assertTrue(all(x in computed_routes for x in hardcoded_routes), 'Expected %s get %s'%(hardcoded_routes, computed_routes))

    def test_get_carrying_nodes(self):
        for i in range(100):
            states = [0 if x < 0.5 else random.randint(1, 4) for x in [random.random() for i in range(5)]]
            s = State(states, 0)
            self.assertEqual(s.get_carrier_nodes(), [i for i in range(5) if states[i] > 0], "")

    def test_one_sender_rules(self):
        for copies in range(1, 4):
            self.one_sender_rules_aux_method(copies)

    def test_many_sender_rules(self):
        print("test_many_sender_rules: We are not checking 3 copies")
        for copies in range(1, 3):
            self.many_sender_rules_aux_method(copies)
        #Commented because of time
        #self.many_sender_rules_aux_method(3)

    def test_listfailures1(self):
        net = Net(2, [Contact(0,1, 1)]) # Dummy net
        bruf = BRUF(net, [0], 1, 1, [], 'working_dir') #Dummy BRUFME
        bruf.state_factory.empty_state_list() #Set up state_factory

        to_state = bruf.state_factory.create_state([0,1], 2) #Dummy to_state
        rules = [OneSenderRule([1], [Route([net.contacts[0]])])] #Dummy rule
        computed_from_ = to_state.gen_previous_state(rules, bruf.state_factory)

        expected_success_from = bruf.state_factory.create_state([1, 0], 1)
        self.assertEqual(expected_success_from.id, computed_from_.id) # Check expected from

        t = Transition(computed_from_, to_state, rules)
        t.compute_changes([], bruf.state_factory)

        #Hardcode expected changes to validate the computed ones
        expected_change_states_list = sorted([to_state.id, bruf.state_factory.create_state([0, 1], 2).id])
        expected_change_case_list = ((1,), (0,))

        sorted_change_list = sorted(t._changes_list)
        for i in range(len(sorted_change_list)):
            c:Change = sorted_change_list[i]

            self.assertEqual(c.to_state.id, expected_change_states_list[i])
            self.assertEqual(1, len(c._cases))
            cc: ChangeCase = c._cases[0]
            self.assertEqual([net.contacts[0].id], [x.id for x in cc._contacts])
            self.assertEqual(expected_change_case_list[i], cc._case)


    def test_check_builded_tree(self):
        pass

    def test_pr1(self):
        pass

    def test_pr2(self):
        pass

    def test_index(self):
        index = Index([0,0,0],[3,10,50])
        for i in range(3):
            for j in range(10):
                for k in range(50):
                    self.assertEqual((i,j,k), index.values)
                    index.next()

        self.assertTrue(index.finish())

    def test_final_states(self):
        net = Net(8, [Contact(0,7,0)]) # empy net

        for copies in range(1, 5):
            for t in range(7):
                bruf = BRUF(net, [0], 3, copies, [], 'working_dir')
                required_final_states = sorted([State.get_identifier(x, 1, copies)
                                        for x in itertools.product(range(copies + 1), repeat=8) if sum(x) == copies and x[t] > 0])
                computed_final_states = []
                for c in range(1, copies + 1):
                    computed_final_states.append([State.get_identifier(s, 1, copies) for s in bruf.gen_final_states(copies - c, t, c)])
                computed_final_states = sorted(list(itertools.chain.from_iterable(computed_final_states)))

                self.assertEqual(required_final_states, computed_final_states, 'FinalState fail For %d copies, target %d'%(copies, t))



class TestIntegration(unittest.TestCase):
    def setUp(self):
        os.system('mkdir working_dir')

    def tearDown(self):
        os.system('rm -r working_dir')

    def test_genfinalprobabilities_1(self):
        net = Net.get_net_from_file('twopath.py')
        bruf = BRUF(net, [0], 3, 2, [], 'working_dir')
        bruf.compute_bruf()

        states = [((2,0,0,0), 0) , ((0,0,0,2), 1), ((0,0,1,1), 1), ((0,1,0,1), 1), ((1,0,0,1),1)]
        max_success_pr = [0.9*0.8 + 0.85 * 0.7 - 0.9*0.8 * 0.85 * 0.7, 1., 1., 1., 1. ]
        max_success_rule = [ [(1, (0,2)), (1, (1,3))], None, None, None, None]

        self.assertEqual(len(states), bruf.states_number())
        for s, pr, msrule in zip(states, max_success_pr, max_success_rule):
            save_s = bruf.get_state(State.get_identifier(s[0], s[1], 2))
            self.assertTrue(isinstance(save_s, SoftState), 'Saved state is not instance of SoftState. It is %s'%(str(s)))
            self.assertEqual(s, (save_s.states, save_s.ts) )
            self.assertAlmostEqual(pr, save_s.get_probability(), delta=0.0000001)
            if msrule is None:
                self.assertEqual(msrule, save_s.max_success_transition())
            else:
                t = save_s.max_success_transition().to_tuple_rules()
                self.assertTrue(all(r in t for r in msrule) and len(msrule) == len(t), "Expected %s Get %s"%(msrule, t))
                to_states = [to.to_state for to in save_s.max_success_transition().change_list]
                for x in [s for s in states if s[1]==1]:
                    self.assertTrue(State.get_identifier(x[0], x[1], 2) in to_states)


    def test_genfinalprobabilities_2(self):
        net = Net.get_net_from_file('twopath_twots.py')
        bruf = BRUF(net, [0], 3, 2, [], 'working_dir')
        bruf.compute_bruf()

        states = [((2,0,0,0), 0), ((0,0,2,0), 1), ((0,2,0,0), 1), ((0,1,1,0), 1), ((1,1,0,0), 1), ((1,0,1,0), 1), ((0,0,0,2), 2), ((0,0,1,1), 2), ((0,1,0,1), 2), ((1,0,0,1),2)]
        max_success_pr = [0.9 * 0.8 + 0.85 * 0.7 - 0.9 * 0.8 * 0.85 * 0.7, 0.7, 0.8, 1 - (0.2 * 0.3), 0.8, 0.7, 1., 1., 1., 1.]
        max_success_rule = [[(1, (0,)), (1, (1,))], [(1,(3,))], [(1,(2,))], [(1, (2,)), (1, (3,))], [(1,(2,))], [(1,(3,))], None, None, None, None]
        states_not_reacheables_from_root = [((0, 0, 0, 2), 1), ((0, 1, 0, 1), 1), ((0, 0, 1, 1), 1), ((1, 0, 0, 1), 1)]
        states_ids = [State.get_identifier(x[0], x[1], 2) for x in states] + [State.get_identifier(x[0], x[1], 2) for x in states_not_reacheables_from_root]
        assert len(states) == len(max_success_pr) == len(max_success_rule), 'Error in test case formulation'
        self.assertEqual(len(states) + len(states_not_reacheables_from_root), bruf.states_number())



        for s, pr, msrule in zip(states, max_success_pr, max_success_rule):
            save_s = bruf.get_state(State.get_identifier(s[0], s[1], 2))
            self.assertTrue(isinstance(save_s, SoftState), 'Saved state is not instance of SoftState. It is %s'%(str(s)))
            self.assertEqual(s, (save_s.states, save_s.ts))
            self.assertAlmostEqual(pr, save_s.get_probability(), delta=0.0000001)
            if msrule is None:
                self.assertEqual(msrule, save_s.max_success_transition())
            else:
                t = save_s.max_success_transition().to_tuple_rules()
                self.assertTrue(all(r in t for r in msrule) and len(msrule) == len(t), "Expected %s Get %s"%(msrule, t))
                to_states = [to.to_state for to in save_s.max_success_transition().change_list]
                for x in to_states:
                    self.assertTrue(x in states_ids)


    def test_genfinalprobabilities_rng_1(self):
        net = Net.get_net_from_file('twopath.py')
        pf_rng = [x/100 for x in range(0, 110, 10)]
        bruf = BRUF(net, [0], 3, 2, pf_rng, 'working_dir')
        bruf.compute_bruf()

        for pf in pf_rng:
            if pf == 0 or pf == 1:
                max_success_rule = [[(1, (0, 2))], None, None, None, None]
            else:
                max_success_rule = [ [(1, (0,2)), (1, (1,3))], None, None, None, None]
            max_success_pr = [(1-pf) * (1-pf) + (1-pf) * (1-pf) - (1-pf) ** 4, 1., 1., 1., 1. ]
            states = [((2, 0, 0, 0), 0), ((0, 0, 0, 2), 1), ((0, 0, 1, 1), 1), ((0, 1, 0, 1), 1), ((1, 0, 0, 1), 1)]

            self.assertEqual(len(states), bruf.states_number())
            for s, pr, msrule in zip(states, max_success_pr, max_success_rule):
                save_s = bruf.get_state(State.get_identifier(s[0], s[1], 2))
                self.assertTrue(isinstance(save_s, SoftState), 'Saved state is not instance of SoftState. It is %s'%(str(s)))
                self.assertEqual(s, (save_s.states, save_s.ts) )
                self.assertAlmostEqual(pr, save_s.get_probability(pf), delta=0.0000001)
                if msrule is None:
                    self.assertEqual(msrule, save_s.max_success_transition(pf))
                # else:
                #     t = save_s.max_success_transition(pf).to_tuple_rules()
                #     self.assertTrue(all(r in t for r in msrule) and len(msrule) == len(t), "Expected %s Get %s"%(msrule, t)) #It fails because two trasition are equally good and it depens on the order of an iteration
                #     to_states = [to.to_state for to in save_s.max_success_transition(pf).change_list]
                #     if pf == 0 or pf == 1:
                #             self.assertTrue(State.get_identifier([1,0,0,1], 1, 2) in to_states)
                #     else:
                #         for x in [s for s in states if s[1]==1]:
                #             self.assertTrue(State.get_identifier(x[0], x[1], 2) in to_states)

if __name__ == '__main__':
    unittest.main()
