import itertools
from brufn.network import State, Transition, Index, NextRule, ManySenderRule, OneSenderRule, SoftState, Route
from typing import List, Set
import os
import gc
from collections import OrderedDict
import simplejson as json
from brufn.utils import print_str_to_file
from brufn.state_factory import StateFactory
#from memory_profiler import profile


class BRUF:
    '''
    Memory efficient BRUF

    Given a net, solve the problem of computing the Best Routing Under Uncertainties
    given n copies for probabilities varying in a given range
    '''

    def __init__(self, net, source:List[int], target:int, num_of_copies, probabilities_rng, working_dir:str):
        self.net = net

        self.source = tuple(source)
        self.target = target
        self.num_of_copies = num_of_copies

        self.probabilities_rng = probabilities_rng[:]
        self.working_dir = working_dir
        self.state_factory = StateFactory(self.num_of_copies, tuple(self.source))

        #self._transitions = []

        #statistical information
        self._transitions_number = 0

    @property
    def transitions_number(self):
        return self._transitions_number

    def states_number(self):
        return self.state_factory.get_number_of_states()

    def _publish(self, from_, to, applied_rule, previous_level):
        if from_ is not None:
            #check if from_ state was already created else create it
            if not any(from_ == x for x in previous_level):
                previous_level.append(from_)
                if sum(from_.states) > self.num_of_copies:
                    raise TypeError("[STATE %d] TS=%d - %s has more copies than the required" % (from_.id, from_.ts, from_.states))

            #Add transition
            t = Transition(from_, to, applied_rule)
            from_.add_transition(t)
            self._transitions_number += 1

    def compute_one_sender_rules(self, source, target, ts, num_of_copies):
        '''
        :param source:
        :param target:
        :param ts:
        :param num_of_copies:
        :return: number of rules num_of_copies - 1, rules (1,..,num_of_copies)
        '''
        computed_routes: List[Route] = self.net.compute_routes(source, target, ts)
        one_sender_one_route: List[OneSenderRule] = list(itertools.chain.from_iterable([x.generate_one_sender_one_route_rules(num_of_copies - 1) for x in computed_routes]))

        one_sender_many_routes_copies_minus1 = []
        one_sender_many_routes_all_copies = []
        combinations:Index = Index([0]*len(one_sender_one_route), [2]*len(one_sender_one_route))
        while not combinations.finish():
            case = combinations.values
            copies = [one_sender_one_route[i].copies[0] for i in range(len(case)) if case[i] > 0]
            sum_copies = sum(copies)
            if 0 < sum_copies <= num_of_copies:
                routes = [one_sender_one_route[i].route[0] for i in range(len(case)) if case[i] > 0]
                if sum_copies < num_of_copies:
                    one_sender_many_routes_copies_minus1.append(OneSenderRule(copies, routes))
                else:
                    one_sender_many_routes_all_copies.append(OneSenderRule(copies, routes))

            combinations.next()

        one_sender_many_routes_all_copies.extend([OneSenderRule([num_of_copies], [r]) for r in computed_routes])

        return len(one_sender_many_routes_copies_minus1), one_sender_many_routes_copies_minus1 + one_sender_many_routes_all_copies

    def compute_many_sender_rules(self, rules_by_sender, num_of_copies):
        many_sender_rules = []
        for rule in itertools.product(*rules_by_sender):
            filtered_rule = [r for r in rule if isinstance(r, OneSenderRule)]
            sum_copies = sum(x.copies_sum for x in filtered_rule)
            if len(filtered_rule) > 1 and 0 < sum_copies <= num_of_copies:
                copies = []
                routes = []
                for i in range(len(filtered_rule)):
                    elem = filtered_rule[i]
                    copies.extend(elem.copies)
                    routes.extend(elem.route)
                many_sender_rules.append(ManySenderRule(copies, routes, next_copies=num_of_copies - sum_copies))

        return many_sender_rules

    def compute_previous_transitions(self, to_state: SoftState, previous_level: List[State]):
        if to_state.ts > 0:
            carrier_nodes = to_state.get_carrier_nodes()
            rules = []
            for carrier in carrier_nodes:
                one_sender_rules = []
                one_sender_rules_by_node = []
                for other in [x for x in range(to_state.num_of_nodes) if carrier != x and self.target != x]:
                    # It does not any sense to compute routes outgoing target node
                    rules_copies_minus_1, routes_from_other = self.compute_one_sender_rules(other, carrier, to_state.ts - 1, to_state.num_of_carrying_copies(carrier))
                    if len(routes_from_other) > 0:
                        one_sender_rules.extend(routes_from_other)
                    if rules_copies_minus_1 > 0:
                        #Add dummy one sender route to consider those case in wich other does not send bundles
                        one_sender_rules_by_node.append([NextRule(-1, 0)] + routes_from_other[:rules_copies_minus_1])

                many_sender_rules = self.compute_many_sender_rules(one_sender_rules_by_node, to_state.num_of_carrying_copies(carrier))
                carrier_rules = [NextRule(carrier, to_state.num_of_carrying_copies(carrier))] + one_sender_rules + many_sender_rules
                rules.append(carrier_rules)

            indexes = Index([0] * len(carrier_nodes), [len(rules[i]) for i in range(len(carrier_nodes))])
            while not indexes.finish():
                applied_rule = [rules[i][indexes.values[i]] for i in range(len(carrier_nodes))]
                previous_state = to_state.gen_previous_state(applied_rule, self.state_factory)
                self._publish(previous_state, to_state, applied_rule, previous_level)
                indexes.next()

    #@profile
    def compute_bruf(self):
        #Initializing next level with the successful finish states
        self.state_factory.empty_state_list()
        final_states = []
        for c in range(1, self.num_of_copies + 1):
            final_states.append([self.state_factory.create_state(s, self.net.num_of_ts)
                                 for s in self.gen_final_states(self.num_of_copies - c, self.target, c)])

        next_level: List[SoftState] = []
        for s in itertools.chain.from_iterable(final_states):
            if len(self.probabilities_rng) > 0:
                s.compute_probability_rng(self.target, self.probabilities_rng)
            else:
                s.compute_probability(self.target)
            next_level.append(s.to_softstate())

        #del final_states
        previous_level: List[State] = []
        for level in range(self.net.num_of_ts, 0, -1):
            #bulding level i-1
            for state_i in next_level:
                #Compute previous state for state_i
                state_i = state_i.to_state()
                self.compute_previous_transitions(state_i, previous_level)


            n_of_states = len(previous_level)
            counter = 1
            aux = []
            self.state_factory.set_state_list(next_level)
            while len(previous_level) > 0:
                current = previous_level.pop()
                print("TS %d - Solve state %d from %d - Transitions number: %d"%(current.ts, counter, n_of_states, len(current.transitions)))
                #for t in current.transitions:
                    #self._transitions.append(t)
                    #t.compute_changes(next_level, self.state_factory)
                if len(self.probabilities_rng) > 0:
                    current.compute_probability_rng(self.target, self.probabilities_rng, next_level, self.state_factory)
                else:
                    for t in current.transitions:
                        # self._transitions.append(t)
                        t.compute_changes(next_level, self.state_factory)
                    current.compute_probability(self.target)

                aux.append(current.to_softstate())
                counter += 1

            previous_level = []

            for s in next_level:
                s.pickle_dump(self.working_dir)

            next_level = aux
            self.state_factory.empty_state_list()


        print("There are %d Initial States"%(len(next_level)))
        for s in self.source:
            copies = [0] * self.net.num_of_nodes; copies[s] = self.num_of_copies
            initial_state = next(filter(lambda x:  x.id == State.get_identifier(copies, 0, self.num_of_copies), next_level), None)
            if initial_state is not None:
                initial_state.pickle_dump(self.working_dir)
                if len(self.probabilities_rng) > 0:
                    computed_probabilities_rng = [(pf, initial_state.get_probability(pf=pf)) for pf in self.probabilities_rng]
                    print("SDP from:%d - to:%d : %s" %(s, self.target, computed_probabilities_rng))
                    print_str_to_file(str(computed_probabilities_rng), os.path.join(self.working_dir, 'f-from:%d-to:%d.txt' %(s,self.target)))
                    for pf in self.probabilities_rng:
                        output_path = os.path.join(self.working_dir, 'mc-from:%d-to:%d-%0.2f.dot' %(s,self.target, pf))
                        #self.mc_to_dot(initial_state.id, output_path, pf=str(pf))
                else:
                    print("SDP from:%d - to:%d : %s" %(s, self.target, initial_state.get_probability()))
                    print_str_to_file(str(initial_state.get_probability()), os.path.join(self.working_dir, 'f-from:%d-to:%d.txt' %(s,self.target)))
                    output_path = os.path.join(self.working_dir, 'mc-from:%d-to:%d.dot' % (s, self.target))
                    self.mc_to_dot(initial_state.id, output_path)
            else:
                print("[WARNING] There is not initial state for traffic from %d to %d"%(s, self.target))

    def mc_to_dot(self, root, output_file, pf=-1):
        output_file = open(output_file, 'w')
        output_file.write("digraph s { \n")
        output_file.write('size="8,5"\n')
        output_file.write( 'node [shape=box];\n')
        states = ""

        to_visit: List[int] = [root]
        visited: Set[int] = set(to_visit)
        while len(to_visit) > 0:
            current: SoftState = self.get_state(to_visit.pop(0))
            copies = str(current.states)

            if current.states[self.target] > 0:
                states += ('%d [label="%d\\n %s - %d "] [peripheries=2];\n' % (current.id, current.id, copies, current.ts))
            else:
                states += ('%d [label="%d\\n %s - %d "];\n' % (current.id, current.id, copies, current.ts))
                if current.max_success_transition(pf = pf) is not None:
                    rules = str([str(rule) for rule in current.max_success_transition(pf=pf).rule])
                    output_file.write( '%d -> n%d_0[arrowhead=none, label = "%s"];\n' %(current.id, current.id, rules))
                    output_file.write( 'n%d_0[shape = point, width = 0.1, height = 0.1, label = ""];\n' % (current.id))
                    for change in current.max_success_transition(pf=pf).change_list:
                        output_file.write( 'n%d_0 -> %d[label = "%f"];\n' % (current.id, change.to_state, change.get_probability(pf=pf)))
                        if change.to_state not in visited:
                            to_visit.append(change.to_state)
                            visited.add(change.to_state)

        output_file.write(states)
        output_file.write('}')
        output_file.close()

    def get_state(self, id: int) -> SoftState:
        return SoftState.pickle_load(self.working_dir, id)

    def gen_final_states(self, copies, target_node, target_copies, prefix=None):
        '''
        Given a number of copies that will be send, it generates all possible final states,
        in which target_node gets target_copies of the traffic.

        :param copies: Number of copies that will be send apart from those gotten for the target node
        :param target_node: Identifier of target node
        :param target_copies: An int, which tells the number of copies that will arrive to target_node
        :param prefix: It is an internal parameter. So it must be call without setting this parameter.
        :return:
        '''
        if prefix is None:
            prefix = []
        if len(prefix) == target_node:
            prefix += [target_copies]
            return self.gen_final_states(copies, target_node, target_copies, prefix=prefix)
        elif len(prefix) == self.net.num_of_nodes - 1:
            return [prefix + [copies]]
        elif len(prefix) + 2 == self.net.num_of_nodes and target_node == self.net.num_of_nodes - 1:
            #Case target_node is the one with highest id
            return [prefix + [copies, target_copies]]
        else:
            result = []
            for i in range(copies + 1):
                new_prefix = prefix + [i]
                result += self.gen_final_states(copies - i, target_node, target_copies, prefix=new_prefix)

            return result


    def generate_mc_to_dtnsim(self, source, output_file, pf:float = -1):
        root = State.get_identifier([0 if i!=source else self.num_of_copies for i in range(self.net.num_of_nodes)]
                                    ,0, self.num_of_copies)
        to_visit: List[int] = [root]
        visited: set[int] = set(to_visit)

        first_dtnsim_state = OrderedDict()
        first_dtnsim_state['id_python'] = -1
        first_dtnsim_state['ts'] = -1
        first_dtnsim_state['copies_by_node'] = [0] * self.net.num_of_nodes
        first_dtnsim_state['actions'] = []
        first_dtnsim_state['children'] = [1]  # Then it is solved to int which means the position of state in list
        first_dtnsim_state['solved_copies'] = 0  # next copies
        first_dtnsim_state['id_dtnsim'] = 0

        dtnsim_states = [first_dtnsim_state]
        mapping_stateid_to_dtnsimid = {-1: 0}

        while len(to_visit) > 0:
            current: SoftState = self.get_state(to_visit.pop())
            copies = str(current.states)

            # Add state to states list
            dtnsim_state = OrderedDict()
            dtnsim_state['id_python'] = current.id
            dtnsim_state['ts'] = current.ts
            dtnsim_state['copies_by_node'] = current.states
            dtnsim_state['actions'] = []
            dtnsim_state['children'] = []  # Then it is solved to int which means the position of state in list
            dtnsim_state['solved_copies'] = 0 # next copies

            mapping_stateid_to_dtnsimid[current.id] = len(dtnsim_states)
            dtnsim_states.append(dtnsim_state)

            if current.states[self.target] == 0:
                #If it isn't a final state, explore children
                if current.max_success_transition(pf) is not None:
                    max_success_transition = current.max_success_transition(pf)
                    dtnsim_state['actions'] = max_success_transition.to_dtnsim_action()
                    dtnsim_state['solved_copies'] = self.num_of_copies - sum([action['copies'] for action in dtnsim_state['actions'] if action['name'] != 'next'])
                    for change in max_success_transition.change_list:
                        dtnsim_state['children'].append(change.to_state)
                        if change.to_state not in visited:
                            to_visit.append(change.to_state)
                            visited.add(change.to_state)

        for i in range(1,len(dtnsim_states)):
            dtnsim_state = dtnsim_states[i]
            dtnsim_state['children'] = [mapping_stateid_to_dtnsimid[state_id] for state_id in dtnsim_state['children']]

        print_str_to_file(json.dumps(dtnsim_states), output_file)

    '''
    def mdp_to_dot(self, source: int, output_file, output_transition_file:str, pf:str = None, node_labels=None):
        root = State.get_identifier([0 if i!=source else self.num_of_copies for i in range(self.net.num_of_nodes)], 0, self.num_of_copies)
        f_node_label = lambda x: node_labels[x] if node_labels != None else str(x)
        f = lambda x: x if pf is None else x[pf]

        def to_unicode(v):
            if v==0:
                return '\u2070'
            elif v == 1:
                return '\u00B9'
            elif v == 2:
                return '\u00B2'
            elif v==3:
                return '\u00B3'
            else:
                raise ValueError('Only v in range(0,10) can be used')

        def f_color(v):
            if v == 1.:
                return "Blue"
            elif v == 0.5:
                return "Green"
            elif v == 0.25:
                return "Red"
            else:
                return "black"

            #else:
            #    raise ValueError('v=%f was not hardcoded'%v)


        output_file = open(output_file, 'w')
        output_transition_file = open(output_transition_file, 'w')
        output_file.write("digraph s { \n")
        output_file.write('size="8,5"\n')
        output_file.write( 'node [shape=box];\n')
        states = ""

        to_visit: List[int] = [root]
        visited: Set[int] = set(to_visit)
        t_number = 0
        while len(to_visit) > 0:
            current: SoftState = self.get_state(to_visit.pop(0))
            copies = ' '.join([f_node_label(i) + to_unicode(v)  for i,v in enumerate(current.states)])

            if current.states[self.target] > 0:
                states += ('%d [label="%d\\n %s - %d "] [peripheries=2];\n' % (current.id, current.id, copies, current.ts))
                #states += ('%d [label=" %s - %d "] [peripheries=2];\n' % (current.id, copies, current.ts))
            else:
                #states += ('%d [label="%s - %d "];\n' % (current.id, copies, current.ts))
                states += ('%d [label="%d\\n %s - %d "];\n' % (current.id, current.id, copies, current.ts))
                if type(current.transitions) == dict and pf in current.transitions.keys():
                    for t_id, transition in enumerate(current.transitions[pf]):
                        rules = str([str(rule) for rule in transition.rule])
                        output_file.write('%d -> n%d_%d[arrowhead=none];\n' % (current.id, current.id, t_id))
                        #output_file.write( '%d -> n%d_%d[arrowhead=none, label = "%s"];\n' %(current.id, current.id, t_id, rules))
                        output_file.write('n%d_%d[shape = plain, width = 0.1, height = 0.1, label="T%d"];\n' % (current.id, t_id,t_number))
                        #output_file.write( 'n%d_%d[shape = point, width = 0.1, height = 0.1, label = ""];\n' % (current.id, t_id))

                        for change in transition.change_list:
                            #output_file.write( 'n%d_%d -> %d[label = "%f"];\n' % (current.id, t_id, change.to_state, f(change.get_probability())))
                            output_file.write('n%d_%d -> %d[color=%s];\n' % (
                            current.id, t_id, change.to_state, f_color(f(change.get_probability()))))
                            if change.to_state not in visited:
                                to_visit.append(change.to_state)
                                visited.add(change.to_state)

                        output_transition_file.write("Transition %d: %s\n"%(t_number,str([str(rule) for rule in transition.rule])))
                        t_number += 1

        output_file.write(states)
        output_file.write('}')
        output_file.close()
        output_transition_file.close()
        '''