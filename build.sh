#!/usr/bin/env bash

cd brufn/test/
python sometests.py
if [[ $? = 0 ]]; then
    cd ../.. &&
    source venv/bin/activate &&
    python setup.py sdist bdist_wheel
    if [[ $? = 0 ]]; then
        echo
        echo "***************************************************"
        echo "                    All was OK                     "
        echo "***************************************************"
        echo
    else
            echo
        echo "***************************************************"
        echo "                Something went wrong               "
        echo "***************************************************"
        echo
    fi

else
    cd ../.. &&
    echo
    echo "***************************************************"
    echo "                   Test Fails                     "
    echo "***************************************************"
    echo
fi


